// Added USPINLOCK per Assign 5 - shared memory for xv6 description
// Mutual exclusion lock.
struct uspinlock {
  uint locked;       // Is the lock held?
  
  // For debugging:
  //char *name;        // Name of lock.
  struct cpu *cpu;   // The cpu holding the lock.
 // uint pcs[10];      // The call stack (an array of program counters)
                     // that locked the lock.
};

// Defined Functions
void uinitlock (struct uspinlock *lock);
void uacquire (struct uspinlock *lock);
void urelease (struct uspinlock *lock);