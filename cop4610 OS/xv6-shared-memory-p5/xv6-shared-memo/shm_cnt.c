#include "types.h"
#include "stat.h"
#include "user.h"
#include "uspinlock.h"


struct shm_cnt{
    struct uspinlock lock;
    volatile int cnt;
};

int main(int argc, char *argv[])
{
    int pid;
    int i;

    struct shm_cnt *base; 
    //struct shm_cnt *cbase; 

    // call fork()
    pid = fork();

    //first parent process will create a page, then child process will open shared page
    base = (struct shm_cnt *) shm_open(0xbeefbeef);

    // 2nd parent calls uinitlock()
    uinitlock(&base->lock); 

    for(i = 0; i < 10000; i++) 
    {
        uacquire(&base->lock);
        base->cnt = base->cnt + 1;
        urelease(&base->lock);

    }

    if(pid) 
    {
        wait();
    } else 
    {}

    printf(1,"For %s the integer is %d at address %x\n",pid? "Parent" : "Child", base->cnt, base);
    
    shm_close(0xbeefbeef);
    exit();
    return 0;
}