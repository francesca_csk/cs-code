#include "types.h"
#include "uspinlock.h"
#include "x86.h"
#include "memlayout.h"
#include "mmu.h"


void uinitlock (struct uspinlock *lock)
{
    // 0 -> lock is available, 1 -> held
    //lock->name = name;
    lock->locked = 0;
    //lock->cpu = 0;

}

void uacquire (struct uspinlock *lock)
{
  // The xchg is atomic.
  while(xchg(&lock->locked, 1) != 0)
    ;

  // Tell the C compiler and the processor to not move loads or stores
  // past this point, to ensure that the critical section's memory
  // references happen after the lock is acquired.
  __sync_synchronize();



}

void urelease (struct uspinlock *lock)
{
   // __sync_synchronize();
   xchg(&lock->locked, 0);
  //lock->locked = 0;
  //lock->cpu = 0;

}