/*******************************************************                                                                                      
 *The Unix Shell
 *******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>


extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;

// data to process execvp - Child PID
pid_t child;
// Exit status of child
int cstatus;
// Pid of child to be returned by wait()
pid_t cp;
// List of arguments for the child process
char *args[3];

char *string1;
char *string2;
char *token;
char *tokensub;
char *sptr1;
char *sptr2;
static char* del = ";";
int i;

void  parse(char *line, char **argv)
{
  static char* delimiter = " \n\t";
  char *token = strtok(line, delimiter);
  while (token != NULL) {
    *argv++ = token;
    token = strtok(NULL, delimiter);
  }

  // to mark the end of argument list
  *argv = (char *)'\0';
} // end of parse func

void  execute(char **argv)
{
     pid_t  pid;
     int    status;

     /* fork a child process */
     if ((pid = fork()) < 0) {
          printf("*** ERROR: forking child process failed\n");
          exit(1);
     }
     else if (pid == 0)
     {
         if (execvp(*argv, argv) < 0) {
               printf("Exec failed on command %s\n", *argv);
              exit(1);
         }
     }
     else
     {     // for the parent: wait for completion
          while (wait(&status) != pid)
               ;
     }
} // end of execute func

int executeBatch(char* filename)
{
    char line[1024];
    char  *argv[64];

    // Handles file pointer infile to process input file
    FILE* infile = fopen(filename, "r");

    if (infile == NULL)
    {
        fprintf(stderr, "Error: File does not exist or cannot be opened.\n");
        return EXIT_FAILURE;
    }

    printf("FILE IS: %s, argv[1] is %s\n", filename, filename);

    // Pos infile to EOF
    fseek(infile, 0, SEEK_END);

    // Check if file has input with file pos
    if (ftell(infile) == 0)
    {
        fprintf(stderr, "Error: No Commands in file!!! \n");
        return EXIT_FAILURE;
    }

    // Set fseek to file beginning
    fseek(infile, 0, SEEK_SET);


    while (fgets(line, sizeof(line), infile))
    {
        if (line[strlen(line) - 1] == '\n')
        {
            char length = strlen(line);
            printf("Retrieved line of length %d :", length);
            // Output command line
            printf("\n+%s\n", line);

            // Outermost for loop to break line into level hierarchy of tokens
            for (i = 1, string1 = line; ; i++, string1 = NULL)
            {
                token = strtok_r(string1, del, &sptr1);
                if (token == NULL)
                    break;

                // Innermost for loop continues level of hierarchy of tokens between ';'
                for (string2 = token; ; string2 = NULL)
                {
                    tokensub = strtok_r(string2, del, &sptr2);
                    if (tokensub == NULL)
                    break;

                    // Call parse function to handle parsed token command/s
                    parse(tokensub, argv);

                    if (strcmp(argv[0], "exit") == 0)
                        exit(0);

                    if (strcmp(argv[0],"quit") == 0 && argv[1] != NULL)
                    {

                        char *token;
                        token = strtok(line, "quit");
                        while (token != NULL) {
                            *argv = token;
                            token = strtok(NULL, " ;");
                        }
                        execute(argv);
                        exit(0);
                    }
                    else if (strcmp(argv[0], "quit") == 0)
                        exit(0);


                    // Call execute function to execute command/s
                    execute(argv);


                    // To handle process ID of the child process
                    // Start with Child process
                    if ((child = fork()) == 0)
                    {
                        //printf("Child: PID of Child = %ld\n", (long) getpid());
                        // arg[0] = command name
                            execvp(args[0], args);

                            //Error if child process reaches this point - thus failed then
                            fprintf(stderr, "Child process could not do execvp.\n");
                            exit(1);
                    }
                    else
                    {
                        // Parent process
                        if (child == (pid_t)(-1))
                        {
                            fprintf(stderr, "Fork failed.\n"); exit(1);
                        }
                        else
                        {
                            // Parent to wait for the child to complete.
                            cp = wait(NULL); // &cstatus
                            printf("PID %ld exited with status %d\n",
                                    (long) cp, cstatus);
                        }
                    } // end of process ID execvp

                } // end of innermost for loop
            } // end of outermost for loop
        } // end if loop
    } // end while loop

    // Close infile
    fclose(infile);

    return 1;
} // end of executeBatch func


int main(int argc, char *inargv[])
{
    char  line[1024];
    char  *argv[64];
    char  *c = NULL;


    if (argc > 2)
    {
        fprintf(stderr, "Warning: Argument Incoming Exceeded! Max{inargv[0] = executing file; inargv[1] = batch file}\n");
        exit(1);
    }

    // Check if shell should run batch mode
    if (argc == 2)
    {
        executeBatch(inargv[1]);
        return 1;
    }

    printf("prompt> ");

    while (fgets(line, sizeof(line), stdin)) // != 0
    {
        // Outermost for loop to break line into level hierarchy of tokens
        for (i = 1, string1 = line; ; i++, string1 = NULL)
        {
            token = strtok_r(string1, del, &sptr1);
            if (token == NULL)
            break;

            // Innermost for loop continues level of hierarchy of tokens between ';'
            for (string2 = token; ; string2 = NULL)
            {
                tokensub = strtok_r(string2, del, &sptr2);
                if (tokensub == NULL)
                break;

                // Call parse function to handle parsed token command/s
                parse(tokensub, argv);

                if (strcmp(argv[0], "exit") == 0)
                    exit(0);

                if (strcmp(argv[0], "quit") == 0 && argv[1] == NULL )
                    exit(0);

                if (argv[0] == NULL)
                    exit(0);

                // Call execute function to execute command/s
                 execute(argv);

                // To handle process ID of the child process
                // Start with Child process
                if ((child = fork()) == 0)
                {
                    //printf("Child: PID of Child = %ld\n", (long) getpid());
                    // arg[0] = command name
                        execvp(args[0], args);

                        //Error if child process reaches this point - thus failed then
                        fprintf(stderr, "Child process could not do execvp.\n");
                        exit(1);
                }
                else
                {
                    // Parent process
                    if (child == (pid_t)(-1))
                    {
                        fprintf(stderr, "Fork failed.\n"); exit(1);
                    }
                    else
                    {
                        // Parent to wait for the child to complete.
                        cp = wait(NULL); // &cstatus
                        printf("PID %ld exited with status %d\n",
                                (long) cp, cstatus);
                    }
                } // end of process ID execvp

            } // end of innermost for loop
        } // end of outermost for loop

        printf("prompt> ");
    } // end of while loop
     return 0;

} // end main function
