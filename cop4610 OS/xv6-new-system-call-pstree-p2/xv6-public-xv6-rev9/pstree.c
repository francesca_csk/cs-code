#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "uproc.h"
   

int main(int argc, char *argv[]) {

  static int MAXSIZE = 64;

	struct uproc up[MAXSIZE];

	int asize = getprocs(MAXSIZE, up);
	
  // Error - empty table 
	if( asize == 0 )
  {
		printf(2, "Error: Empty table - problem storing necessary information.\n");

	  exit(); // exit if problem with array data objects
  }
	
   //	int i = 0;

  // Start for loop to go through table structure
  int i = 0; 
  while (i < asize)
  {  
    // if ppid == 0 then init root 
    if (up[i].ppid == 0 && up[i].pid == 1) 
    {
      printf(1,"%s[%d]\n", up[i].name, up[i].pid);
    }
    // else if ppid == 1 then child process (..sh)
    else if (up[i].ppid == 1) 
    {
       printf(1,"   %s[%d]\n",up[i].name, up[i].pid);

    }
    // else if ppid > 1 then grandchildren 
    else if (up[i].ppid > 1 )
    {
       printf(1,"      %s[%d]\n",up[i].name, up[i].pid);

    }

    i++;


  } // end for loop

   exit();

 } // end main function for pstree - user application
