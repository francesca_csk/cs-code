// uproc.h header file for getprocs() system call & pstree user app
struct uproc {
    int pid;
    int ppid;
    char name[16];  

    // added uproc state for pstree.c
    char uprocsta[16];   
 };

