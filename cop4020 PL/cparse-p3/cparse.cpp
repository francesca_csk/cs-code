/*                                                                              
 *  Compile: "g++ -std=c++11 -o cparse schroff.cpp"
 *      Run: ./cparse < cparse.in
 */

#include <iostream>
#include <stdio.h> // fopen
#include <cstdlib>
#include <fstream> // ifstream
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <iterator>

struct parser {
    std::vector<parser*> augmentProduc;

    std::map<char, int> gotoAct;

    // default constructor
    parser ();

    //parser( std::vector<parser>* v){ augmentProduc = v;}
    // 2-arg constructor
    parser(char, std::string);

    // operator overload func []
    parser* operator[] (const int);

    char NonTerm; // NonTerm for each set contains 1 NONTerm

    std::string sideR; // sideR can contain a sequence of both NONTerm and Terms symbols

    // check production to see if already present or not
    bool ifPresent(std::string);

    // add to parser function
    void pars_pushback(parser *);

    // check parser size function
    int checkSetsize();
};

// default constructor
parser::parser()
{}

// 2-arg constructor
parser::parser(char lSide, std::string rSide)
{
    NonTerm = lSide;
    sideR = rSide;
}

// operator overload func []
parser* parser::operator[] (const int i)
{
    parser* produc;

    produc = augmentProduc[i];

    return produc;
}

// check production to see if already present or not
bool parser::ifPresent(std::string nonterm)
{
    for (std::vector<parser*>::iterator iter = augmentProduc.begin(); iter != augmentProduc.end(); iter++)
    {
        std::string present;

        std::string rarrow = "->";

        present = std::string(&(*iter)->NonTerm) + rarrow + (*iter)->sideR;

        // if production already present return 1
        if (nonterm.compare(present) == 0) return 1;
    }
    return 0;

}

// add set to parser function
void parser::pars_pushback(parser *item)
{
    augmentProduc.push_back(item);
}

// check parser size function
int parser::checkSetsize()
{
    int getSize = augmentProduc.size();

    return getSize;
}

typedef std::map<std::string, int> gotoOp;

typedef std::map<char, std::vector<std::string> > augmentOp;

// cparse main function
int main(int argc, char *argv[])
{
    // initialize data
    int nextpos;
    int nextItem;
    int loc;
    int ps;
    int isets;
    int i;
    int j;
    int size;
    int z= 0;
    int nc = 1;

    char peek;
    char checkPeek;
    char leftSide;
    char s = 39;

    std::string newrightSide;
    std::string producSet;
    std::string rightSide;
    std::string llSide;
    std::string rSide;
    std::string sSide;
    std::string rs;

    std::string gp;
    std::string nonTerm;
    std::string ritems;
    std::string posit = "@";
    std::string delim = "->";

    augmentOp augmGrmr;

    std::vector<parser> setsLR;

    setsLR = {parser()};

    gotoOp setsGoto;

    std::cin >> nonTerm;

    augmGrmr[s].push_back(nonTerm);

    setsLR[z].pars_pushback(new parser(s, posit + nonTerm));

    // Prepare first output - Augmented Grammar
    std::cout << "Augmented Grammar\n";
    std::cout << "-----------------\n";

    std::cout << "'->" << nonTerm << '\n';

    while(std::cin >> gp)
    {
        if (gp.length() >= nc)
        {
            loc = gp.find(delim);

            if(loc == std::string::npos)
            {
              augmGrmr[nonTerm[z]].push_back(ritems);

              std::cout << nonTerm << "->" << ritems << '\n';

              setsLR[z].pars_pushback(new parser(nonTerm[z], posit + ritems));
            }
            else
            {
                nonTerm = gp.substr(z,loc);

                ritems = gp.substr(loc + delim.length(), std::string::npos);

                augmGrmr[nonTerm[z]].push_back(ritems);

                std::cout << nonTerm << "->" << ritems << '\n';

                setsLR[z].pars_pushback(new parser(nonTerm[z], posit + ritems));
            }
        }
    }

    std::cout << '\n';

    // Prepare second output - Sets of LR(0) Items
    std::cout << "Sets of LR(0) Items\n";
    std::cout << "-------------------\n";

    for ( isets = 0; isets < setsLR.size(); ++isets)
    {
       std::cout << "I" << isets << ":\n";

        // check for closure operation
        for ( i = 0; i < setsLR[isets].checkSetsize(); i++)
        {
            sSide = setsLR[isets][i]->sideR;

            rSide = sSide;

            checkPeek = rSide[rSide.find(posit) + nc];

            // if peek(next char to read) is a NONTerm
            if (isupper(checkPeek))
            {

                // peek(next char to read) == NONTerm
                llSide = checkPeek;

                // closure operation check production for all items
                for ( j =  0; j < augmGrmr[checkPeek].size(); j++)
                {
                    rs = posit + augmGrmr[checkPeek][j];

                    std::string currset = llSide + delim + rs;

                    if (setsLR[isets].ifPresent( currset ))
                    {

                    }
                    else
                    {
                        setsLR[isets].pars_pushback(new parser(checkPeek, rs));
                    }


                } // end of inner for loop
            }
        } // end of outer/inner for loop

        int i = -1;

        while (++i < setsLR[isets].checkSetsize())
        {
            leftSide = setsLR[isets][i]->NonTerm;

            rightSide = setsLR[isets][i]->sideR;

            producSet = leftSide + delim + rightSide;

            // check next char to read or if end $
            peek = rightSide[rightSide.find(posit) + nc];

            if (peek != z)
            {
                // assign goto for set if none specified
                if (setsLR[isets].gotoAct.find(peek) != setsLR[isets].gotoAct.end())
                {
                    ps = rightSide.find(posit);

                    std::swap(rightSide[ps], rightSide[ps + nc]);

                    nextItem = setsLR[isets].gotoAct[peek];

                    // store temporary production set
                    std::string nstr = leftSide + delim + rightSide;

                    // see if production already present
                    if (setsLR[nextItem].ifPresent(nstr))
                    {
                        std::swap(rightSide[ps], rightSide[ps + nc]);

                        // item without a goto
                        printf("   %-20s\n", &producSet[z]);

                    }
                    else
                    {
                        setsLR[nextItem].pars_pushback(new parser(leftSide, rightSide));

                        std::swap(rightSide[ps], rightSide[ps + nc]);

                        // else item without a goto
                        printf("   %-20s\n", &producSet[z]);
                    }

                } // end of outer if statement
                else if (setsLR[isets].gotoAct.find(peek) == setsLR[isets].gotoAct.end())
                {
                    if (setsGoto.find(producSet) != setsGoto.end())
                    {
                        // else employ present item
                        setsLR[isets].gotoAct[peek] = setsGoto[producSet];

                    } // end of inner if statement
                    else if (setsGoto.find(producSet) == setsGoto.end())
                    {
                        setsLR.push_back(parser());

                        newrightSide = rightSide;

                        int atpos = newrightSide.find(posit);

                        std::swap(newrightSide[atpos], newrightSide[atpos + nc]);

                        // put in item and update vector
                        setsLR.back().pars_pushback(new parser(leftSide, newrightSide));

                        setsLR[isets].gotoAct[peek] = setsLR.size()- nc;

                        setsGoto[producSet] = setsLR.size()- nc;

                    } // end of inner else if statement

                    // item with a goto
                    printf("   %-20s goto(%c)=I%d\n", &producSet[z], peek, setsGoto[producSet]);


                } // end of outer else statement
            }// end outer most if statement
            else
            {

                 // item without a goto
                printf("   %-20s\n", &producSet[z] );

            }


        } // end of inner while loop

        std::cout << '\n';
    } // end main outer for loop

    return 0;

} // end main function/end cparse
