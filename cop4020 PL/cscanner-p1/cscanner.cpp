/*                                                                              
 *  Implementing a C Scanner - Lexical Analyzer
 *  Compile: "g++ -std=c++11 -o cscan.x schroff.cpp"
 *      Run: ./cscan.x < sample.c
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cassert>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <iterator>


#define MAXTOK 256 /* maximum token size */

int cur;    /* current character being processed */
int peek;   /* next charcter to be processed */

int tokcount = 1;
int symbolCount = 0;

std::map<int, std::string> tokens; // temporary map to hold number, string, ident, char tokens
std::map<std::string, int > sym_tokens; // temporary map to hold rest/other symbol tokens
std::map<std::string, int>::iterator it;


struct Compare {
    std::string str;
    Compare(const std::string& str) : str(str) {}
};

bool operator==(const std::pair<int, std::string>&pa, const Compare& comp)
{
    return comp.str == pa.second;
}

bool operator==(const Compare& comp, const std::pair<int, std::string>&pa)
{
    return comp.str == pa.second;
}
// to sort in descending order of frequency
bool sortby(const std::pair<std::string,int> &xpair,
                   const std::pair<std::string,int> &ypair)
{
    if (xpair.second > ypair.second)
    {
        return xpair.second > ypair.second;
    }
    if (xpair.second == ypair.second)
    {
        return (xpair.first > ypair.first);
    }
    else
     return 0;
}
// to sort same freq by str depending on length (e.g. number, string, ident, char...)
bool strsort(const std::pair<std::string,int> &xpair,
               const std::pair<std::string,int> &ypair)
{
       return (xpair.first > ypair.first);
}
// to handle other tokens - symbols
int isOtherToken(std::string str){
    if (str == "(" || str == ")" || str == "," || str == "." || str == ":" || str == ";"
      || str == "[" || str == "]" || str == "{" || str == "̃" || str == "&&" || str == "||" || str == "++"
      || str == "--" || str == "->"
      ||
        ( str == "|" || str == "ˆ" || str == "&" || str == "+" || str == "-" || str == "%" || str == "*" || str == "/" ||
        str == "=" || str == "!" || str == ">>" || str == "<" || str == "<<"))
        {
            return 1;
        } else
            return 0;
}

int skip() {
  // processes skipping over whitespaces
  while (isspace(cur)) {
    cur = peek;
    peek = std::fgetc(stdin);
  }
  return cur;
}

int ignoreComments(int cur) {
    // processes comments by ignoring them
    while (cur != 10)
    {
        cur = peek;
        peek = std::fgetc(stdin);
    }
    return peek;
}

int scan(char *lexeme) {

    int i = 0;
    tokcount++;

    // skips over whitespaces & EOF check
    if (skip() == EOF)
        return EOF;

    else if (isalpha(cur) || cur == '_') {
        // ID TOKEN
        while (isalpha(cur) || isdigit(cur) || cur == '_') {

            lexeme[i++] = cur;
            cur = peek;
            if (peek != EOF)
                peek = std::fgetc(stdin);
        }

        tokcount++;
        tokens[tokcount] = "ident";

        lexeme[i] = '\0';
        return i;
    }
    else if (cur == 39 && cur != EOF) // 39 = " ' " or alternative (cur == '\'')
    {
        // CHAR TOKEN
        while (cur == 39 || isalpha(cur) || isdigit(cur) || cur == 92 ) // 92 == "\"
        {
            lexeme[i++] = cur;
            cur = peek;
            if (peek != EOF)
                peek = std::fgetc(stdin);
        }

        tokcount++;
        tokens[tokcount] = "char";
        lexeme[i] = '\0';
        return i; // return any value other than EOF
    }
    else if (cur == 34 && cur != EOF) // 34 =  " or alternative (cur == ' " ')
    {
        // STRING TOKEN
        while (cur == '\"' || isalpha(cur) || isdigit(cur) || cur == 92  || ispunct(cur) || cur == 32 ) // 92 == "\"
        {
            lexeme[i++] = cur;
            cur = peek;
            if (peek != EOF)
                peek = std::fgetc(stdin);

            // to process end of string - to make sure string stops after second ditto
            if (cur == ')')
            {
                tokcount++;
                tokens[tokcount] = "string";
                lexeme[i] = '\0';
                return i;
            }
        }
        tokcount++;
        tokens[tokcount] = "string";

        lexeme[i] = '\0';
        return i;
    }
    else if (cur != EOF && isdigit(cur))
    {
        // NUMBER TOKEN
        while (isdigit(cur))
        {
            lexeme[i++] = cur;
            cur = peek;
            if (peek != EOF)
                peek = std::fgetc(stdin);
        }

        tokcount++;
        tokens[tokcount] = "number";

        lexeme[i] = '\0';
        return i;
    }
    else if(ispunct(cur))
    {
        if (cur == '/')
        {
            // if line comment - ignore line comment
            if (peek == '/')
            {
                peek = ignoreComments(cur);
                cur = peek;
                peek = std::fgetc(stdin);

            }
            // if enclosed /* and */ comment - ignore line comment
            else if (peek == '*')
            {
                peek = ignoreComments(cur);
                cur = peek;
                peek = std::fgetc(stdin);

            }
        }
        // if not comment - process token symbol
        lexeme[i++] = cur;
        cur = peek;
        if (peek != EOF)
            peek = std::fgetc(stdin);

        lexeme[i] = '\0';
        return i; // return any value other than EOF
    }
    else {
        // error output
        std::cerr << " Error output - token can't be identified." << std::endl;

		exit(1);
    }
    return 0;
}

int main(int argc, char *argv[])
{
    char lexeme[MAXTOK];
    int  result;

    int symcount = 0;

    int numtemp;
    int strtemp;
    int idetemp;
    int chatemp;

    int symtemp;

     /* setup for scanning */
    cur = peek = std::fgetc(stdin);

    if (cur != EOF)
        peek = std::fgetc(stdin);

    while ((result = scan(lexeme)) != EOF) {

        std::cout << lexeme << std::endl;

        if (isOtherToken(lexeme))
        {
            std::string str(lexeme);

            it = sym_tokens.find(str);

            // if already present in map
            if (it != sym_tokens.end()){
                it -> second++;
            }
            else{
                sym_tokens.insert(std::make_pair(str, 1)); // insert new symb token
            }

        }
    }

    numtemp = count(tokens.begin(), tokens.end(), Compare("number"));
    strtemp = count(tokens.begin(), tokens.end(), Compare("string"));
    idetemp = count(tokens.begin(), tokens.end(), Compare("ident"));
    chatemp = count(tokens.begin(), tokens.end(), Compare("char"));


     // created empty vector to hold map pairs
    std::vector <std::pair<std::string, int> > vecpair;

    // copying key val pairs from sym_tokens map to vector
    std::copy(sym_tokens.begin(), sym_tokens.end(), std::back_inserter<std::vector<std::pair<std::string, int > > >(vecpair));

    vecpair.push_back(std::make_pair("number", numtemp));
    vecpair.push_back(std::make_pair("string", strtemp));
    vecpair.push_back(std::make_pair("ident", idetemp));
    vecpair.push_back(std::make_pair("char", chatemp));


    sort(vecpair.begin(), vecpair.end(), sortby);

    printf("        token          count\n");
    printf("---------------------  -----\n");

    for (auto it = vecpair.begin(); it != vecpair.end(); it++)
        std::cout << std::setw(21) << (*it).first << std::setw(6) <<  (*it).second << '\n';


    return 0;
}
