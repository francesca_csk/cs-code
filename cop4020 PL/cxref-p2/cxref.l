/* lexical analysis for a C cross ref listing  */


/* Definitions/Declarations*/
%{
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <ctype.h>
#include <string>
#include <map>
#include <algorithm>
#include <iterator>
#include <vector>

std::multimap<std::string,int> idmap;

#define CHAR    256
#define NUM     257
#define IDENT   258
#define INVALID 259
#define KEYWORD  258

int character = 0;
int keywords = 0;
int strings = 0;
int pproc = 0;
int comment = 0;
int multicomment = 0;
int string_context = 0;
int line_context = 0;
int star_comment = 0;
int multi_context = 0;
int check_comm = 0;
int number_line = 1;
int singlechar = 0;

void lexical_error(char *);
%}

quote    \'
back     \\
ditto    \"
octal    [0-7]
octch    ({back}{octal}|{back}{octal}{octal}|{back}{octal}{octal}{octal})
digit    [0-9]
alpha    [a-zA-Z_]
schar    [^\'\"\n\\]
sym      [<><=>&\+\[\]]
char     ({back}?({schar}|{ditto})|{back}{back}|{back}{quote}|{octch})
preproc  ^#[a-zA-z  \t0-9<>.]*

/* Rules */
%%
auto        { keywords = !keywords; }
break       { keywords = !keywords; }
case        { keywords = !keywords; }
char        { keywords = !keywords; }
const       { keywords = !keywords; }
continue    { keywords = !keywords; }
default     { keywords = !keywords; }
do          { keywords = !keywords; }
double      { keywords = !keywords; }
else        { keywords = !keywords; }
enum        { keywords = !keywords; }
extern      { keywords = !keywords; }
float       { keywords = !keywords; }
for         { keywords = !keywords; }
goto        { keywords = !keywords; }
if          { keywords = !keywords; }
int         { keywords = !keywords; }
long        { keywords = !keywords; }
register    { keywords = !keywords; }
return      { keywords = !keywords; }
short       { keywords = !keywords; }
signed      { keywords = !keywords; }
sizeof      { keywords = !keywords; }
static      { keywords = !keywords; }
struct      { keywords = !keywords; }
switch      { keywords = !keywords; }
typedef     { keywords = !keywords; }
union       { keywords = !keywords; }
unsigned    { keywords = !keywords; }
void        { keywords = !keywords; }
volatile    { keywords = !keywords; }
while       { keywords = !keywords; }

{quote}  { character = !character; }


{ditto}  { strings = !strings; }
{preproc}  {;}


" "     {;}

\/\/ { if (!string_context){
         comment = !comment;
         line_context = 1; }
      }

\/\* { if (!multi_context){
          check_comm = 0;
           multicomment = !multicomment;
         star_comment = 1; }
      }

\*\/  { if (star_comment) {
          multicomment = 0;
          check_comm = 1;}
      }

\'+{char}+/\^      {singlechar = !singlechar;}


{alpha}({alpha}|{digit})*      { if ((!character) && (!singlechar) && (!strings) && (!comment) && (!multicomment)) return IDENT; }



<<EOF>>                         { return EOF;  }

\n  ++number_line;


.    { ; }
%%

/* C++ functions */

int main() {

    int tok;

    while ((tok = yylex()) != EOF) {

      if (IDENT){

        idmap.insert(std::pair<std::string,int>(yytext,number_line));

      }

   }

    for (std::multimap<std::string, int>::iterator val = idmap.begin();
        val != idmap.end(); ++val)
    {
        val = idmap.find((*val).first);
        int number = idmap.count((*val).first);

        int i = 1;

        std::cout << std::setw(7) << (*val).first << ": ";
        std::vector<int> line;


        for (;i < number; i++)
        {

            if (std::count(line.begin(), line.end(), val->second) < 1 )
            {
                line.push_back(val->second);
                std::cout << val->second << ", ";

            }
            ++val;

        }

        if (i == number)
        {
            std::cout << val->second;
        }

        std::cout << '\n';

    }
    return 0;
}


void lexical_error(char *msg) {
  printf("%s\n", msg);
}
