%{
#include <stdio.h>
#include <string.h>
#include <math.h>

int linenum = 1;

long long int yylex();
long long int vstack[26];

void reset();
void dump();
void yyerror(char *);

%}

%union {
  long long int val;
  char *string;
}

%token <string> DUMP RESET
%token <val> VAL VAR EQUAL BITOR BITXOR BITAND LSHIFT RSHIFT ADD SUB MULTI DIV REM NEG BITNOT INC DEC POPEN PCLOSE
%type <val> equal bor bxor band l_r_shifts oper_add_sub oper_mult_div_rem neg bnot pref_inc_dec postf_inc_dec pars pval


%%

prog	:
		|	prog stmts
		;

stmts		:	equal ';'  			{ 
										if($1 == 0)
                                        	printf("unknown");
                                     	else printf("%lld\n", $1); 
									}
        	|   	DUMP       			{ dump(); }
       		|   	RESET     			{ reset(); }
		;

equal 	: 	VAR EQUAL equal 			{ vstack[$1] = $3; $$ = vstack[$1]; } 
    		| 	bor           			
    		;

bor
    		: 	bxor
    		| 	bor BITOR bxor 	{ $$ = $1 | $3; }
    		;

bxor
    		: 	band
    		| 	bxor BITXOR band 	{ $$ = $1 ^ $3; }
    		;

band
    		: 	l_r_shifts
    		| 	band BITAND l_r_shifts          { $$ = $1 & $3; }
    		;

l_r_shifts
    		:	oper_add_sub
    		| 	l_r_shifts LSHIFT oper_add_sub 		{ $$ = $1 << $3; }
    		| 	l_r_shifts RSHIFT oper_add_sub		{ $$ = $1 >> $3; }
    		;

oper_add_sub
    		: 	oper_mult_div_rem
    		| 	oper_add_sub ADD oper_mult_div_rem 		{  $$ = $1 + $3; }
    		| 	oper_add_sub SUB oper_mult_div_rem 		{  $$ = $1 - $3; }
    		;

oper_mult_div_rem
    		: 	neg
    		| 	oper_mult_div_rem MULTI neg 	{ $$ = $1 * $3; }
    		| 	oper_mult_div_rem DIV neg		{ $$ = $1 / $3; }
    		| 	oper_mult_div_rem REM neg		{ $$ = $1 % $3;} 
    		;

neg
    		: 	bnot
    		| 	SUB bnot 			{ $$ = - $2; }
	    	;

bnot
		: 	pref_inc_dec
    		| 	BITNOT pref_inc_dec		{ $$ = ~ $2; }
    		;

pref_inc_dec :  INC pref_inc_dec		{ vstack[$2] = $2 +1; $$ = vstack[$2]; }
			 |	DEC pref_inc_dec  		{ vstack[$2] = $2 -1; $$ = vstack[$2]; } 
    		 | 	postf_inc_dec           	 
			;

postf_inc_dec
			:	pars
			| 	postf_inc_dec INC  	{  $$ = vstack[$1]; vstack[$1] += 1; }	
			| 	postf_inc_dec DEC 	{  $$ = vstack[$1]; vstack[$1] = $1 -1; }
    		;

pars
    		: 	pval
    		| 	POPEN bor PCLOSE   	{ $$ = $2; }
    		;

pval
    		: 	VAL             			{ $$ = $1; }
    		| 	VAR             			{ $$ = vstack[$1]; }
    		;

%%

int main(int argc, char **argv)
{
   FILE *input = stdin;

   if (yyparse())
      printf("\nInvalid expression.\n");
   else
      printf("\nCalculator off.\n");

    return 0;
}

void yyerror(char *s)
{
    fprintf(stderr, "%d.: %s\n", linenum, s);
}

void dump()
{
    for (int i = 0; i < 26; i++) 
    {
        if (vstack[i] > 0)
            printf("%c: %d\n", (97+i), vstack[i]);
        else if (vstack[i] == 0)
            printf("%c: unknown\n", (97+i));
    }
}

void reset()
{
    int i;
    for (i = 0; i < 26; i++) 
    {
        vstack[i] = 0;
    }
}