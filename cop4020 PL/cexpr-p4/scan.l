%{
#include "y.tab.h"

extern int linenum;
%}

%%

[0-9]+  { sscanf(yytext, "%d", &yylval.val);
          return VAL; }

[a-z]  { yylval.val = *yytext - 'a';
          return VAR; }

"dump"  { yylval.string=strdup(yytext);
          return DUMP; }

"reset" { return RESET; }

"="    	{ return  EQUAL; }
"|"     { return  BITOR; }
"^"     { return  BITXOR; }
"&"     { return  BITAND; }
"<<"    { return  LSHIFT; }
">>"    { return  RSHIFT; }
"+"     { return  ADD; }
"-"     { return  SUB; }
"*"     { return  MULTI; }
"/"     { return  DIV; }
"%"     { return  REM; }
"-"     { return  NEG; }
"~"     { return  BITNOT; }
"++"    { return  INC; }
"--"    { return  DEC; }
"("     { return  POPEN; }
")"     { return  PCLOSE; }

[ /t]+         ;

\n        { linenum++;}
[ \t\r\n]      ;

.       { return yytext[0]; }

%%
